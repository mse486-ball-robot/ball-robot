/*
 * adc_ballplate.h
 *
 *  Created on: Apr 3, 2019
 *      Author: Richard
 */

#ifndef INCLUDE_ADC_BALLPLATE_H_
#define INCLUDE_ADC_BALLPLATE_H_

void setup_adc();
void read_adc();


#endif /* INCLUDE_ADC_BALLPLATE_H_ */
