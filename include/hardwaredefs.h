/*
 * hardwaredefs.h
 *
 *  Created on: Mar 15, 2019
 *      Author: Richard
 */

#ifndef INCLUDE_HARDWAREDEFS_H_
#define INCLUDE_HARDWAREDEFS_H_

#define SERVO_PWM_MODULE PWM1_BASE
#define SERVO_X_PORT PWM_OUT_1
#define SERVO_Y_PORT PWM_OUT_0



#endif /* INCLUDE_HARDWAREDEFS_H_ */
