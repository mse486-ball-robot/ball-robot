/*
 * isr.h
 *
 *  Created on: Feb 5, 2019
 *      Author: Richard
 */

#ifndef INCLUDE_ISR_H_
#define INCLUDE_ISR_H_

void uart0_isr(void);
void uart1_isr(void);

#endif /* INCLUDE_ISR_H_ */
