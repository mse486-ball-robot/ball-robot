/*
 * print.h
 *
 *  Created on: Feb 5, 2019
 *      Author: Richard
 */

#ifndef INCLUDE_PRINT_H_
#define INCLUDE_PRINT_H_

void println(char * buf);

#endif /* INCLUDE_PRINT_H_ */
