/*
 * servo.h
 *
 *  Created on: Mar 15, 2019
 *      Author: Richard
 */

#ifndef INCLUDE_SERVO_H_
#define INCLUDE_SERVO_H_


void setup_pwm();
void set_angle(uint32_t ui32PWMOut, uint32_t adjust);


#endif /* INCLUDE_SERVO_H_ */
