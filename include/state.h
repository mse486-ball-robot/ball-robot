/*
 * state.h
 *
 *  Created on: Mar 15, 2019
 *      Author: Richard
 */

#ifndef INCLUDE_STATE_H_
#define INCLUDE_STATE_H_

#include <stdint.h>

/* State Numbers */
#define IDLE_STATE      0
#define PARSE_STATE     1
#define UPDATE_STATE    2
#define JOYSTICK_STATE    3
#define NONE            4

typedef struct{
    uint8_t joystick_mode;
    uint8_t angle_x_prev;
    uint8_t angle_y_prev;
    uint8_t angle_x_curr;
    uint8_t angle_y_curr;
    int32_t pos_x_prev;
    int32_t pos_y_prev;
    float pos_x_curr;
    float pos_y_curr;
    uint8_t buf_idx;
    float gain[8];
    uint8_t idx;
    uint32_t x_joystick;
    uint32_t y_joystick;
    uint8_t buf[30];
    uint32_t x_offset;
    uint32_t y_offset;
} context_t;

extern context_t context;
void init_context();
void context_buf_append(uint8_t byte);


/* State Machine Guts */
void state_loop();  // Checks the state and runs the appropriate state function
void change_state(uint8_t state_to_enter);  // Call this to change to a new state

#endif /* INCLUDE_STATE_H_ */
