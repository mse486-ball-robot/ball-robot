/**
 * main.c
 */
#include <stdint.h>
#include <stdbool.h>
#include "hw_memmap.h"
#include "hw_types.h"
#include "hw_ints.h"
#include "sysctl.h"
#include "interrupt.h"
#include "gpio.h"
#include "pin_map.h"
#include "uart.h"
#include "print.h"
#include "pwm.h"
#include "debug.h"
#include "hw_gpio.h"
#include "servo.h"
#include "hardwaredefs.h"
#include "state.h"
#include "printf.h"
#include "adc.h"

int main2(void){
    // Setup Clocks and Peripherals
    SysCtlClockSet(
            SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
                    | SYSCTL_XTAL_16MHZ); // 80 MHz main clock

    uint32_t ui32ADC0Value[4];
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    ADCHardwareOversampleConfigure(ADC0_BASE,64);
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH2 ); //sw
    ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_CH0 );
    ADCSequenceStepConfigure(ADC0_BASE, 1, 2, ADC_CTL_CH1 );
    //sample ADC_CTL_TS and configure interrupt flag ADC_CTL_IE to be set when the sample is done. Tell ADC this is the end of conversion
    ADCSequenceStepConfigure(ADC0_BASE,1,3,ADC_CTL_CH0|ADC_CTL_CH1 |ADC_CTL_IE|ADC_CTL_END);


    //end of configuration
    //enable ADC sequencer 1
    ADCSequenceEnable(ADC0_BASE, 1);

    // Must enable the UART and the GPIO port its pins are co-located on
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);        // Debug UART
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);        // Debug UART co-location
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);        // Ball UART
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);        // Ball UART co-location
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);        // Buttons

    setup_pwm();

    // Setup Button Inputs (PF0, PF4)
    HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
    HWREG(GPIO_PORTF_BASE + GPIO_O_CR) |= 0x01;
    HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = 0;
    GPIODirModeSet(GPIO_PORTF_BASE, GPIO_PIN_4 | GPIO_PIN_0, GPIO_DIR_MODE_IN);
    GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4 | GPIO_PIN_0,
                     GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);


    // Set pin types to UART (UART1 only needs RX)
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);

    GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0);
    GPIOPinConfigure(GPIO_PB0_U1RX);

    // Setup UARTs
    UARTConfigSetExpClk(
            UART0_BASE, SysCtlClockGet(), 115200,
            (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
    UARTConfigSetExpClk(
            UART1_BASE, SysCtlClockGet(), 57600,
            (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));

    // Interrupt configuration
    IntEnable(INT_UART0);
    UARTIntDisable(UART0_BASE, UART_INT_TX);
    UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);

    IntEnable(INT_UART1);
    UARTIntDisable(UART1_BASE, UART_INT_TX);
    UARTIntEnable(UART1_BASE, UART_INT_RX | UART_INT_RT);

    IntMasterEnable();

    println("Hello world!");
    init_context();
    while (1){


       /* TODO: Read ADCs here, put value into context.something */
        //clear flag first
          ADCIntClear(ADC0_BASE, 1);
          // trigger ADC
          ADCProcessorTrigger(ADC0_BASE, 1);
          //wait until done
          while(!ADCIntStatus(ADC0_BASE, 1, false))
          {
          }
          //now that the conversion is complete we can read it
          ADCSequenceDataGet(ADC0_BASE, 1, ui32ADC0Value);
          context.sw=(ui32ADC0Value[2]);
         context.y_read = (ui32ADC0Value[1]);
         context.x_read = (ui32ADC0Value[0]);

       state_loop();
    }

    return 0;
}

