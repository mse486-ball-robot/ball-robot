# Ball Robot
CCS project for MSE 483 ball balancing robot demonstration

# Installation
- Clone this repository
- Open up CCS and if necessary, create a new workspace folder. The workspace folder must not be within the `ball-robot` directory, it should be outside somewhere.   
- `File > Import Projects From Filesystem` and navigate to the `ball-robot` direcotry
- CCS should recognize a project called `ball-robot`

# Current Features
- Interrupt-driven UART loopback 
- A simple print function
- Tivaware inluded in the project for portability
