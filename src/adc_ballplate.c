/*
 * adc_ballplate.c
 *
 *  Created on: Apr 3, 2019
 *      Author: Richard
 */
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include "hw_memmap.h"
#include "sysctl.h"
#include "hw_types.h"
#include "adc_ballplate.h"
#include "state.h"
#include "adc.h"

void setup_adc()
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    ADCHardwareOversampleConfigure(ADC0_BASE, 64);
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH0);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_CH1);
    //sample ADC_CTL_TS and configure interrupt flag ADC_CTL_IE to be set when the sample is done. Tell ADC this is the end of conversion
    ADCSequenceStepConfigure(
            ADC0_BASE, 1, 2,
            ADC_CTL_CH0 | ADC_CTL_CH1 | ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceEnable(ADC0_BASE, 1);    // Enable ADC sequencer 1
}

void read_adc()
{
    uint32_t ui32ADC0Value[4];
    ADCIntClear(ADC0_BASE, 1);
    ADCProcessorTrigger(ADC0_BASE, 1);      // trigger ADC
    while (!ADCIntStatus(ADC0_BASE, 1, false))
    {
    }

    // Conversion Complete, use it
    ADCSequenceDataGet(ADC0_BASE, 1, ui32ADC0Value);
    context.y_joystick = (ui32ADC0Value[1]);
    context.x_joystick = (ui32ADC0Value[0]);
}
