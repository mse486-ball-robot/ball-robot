/*
 * control.c
 *
 *  Created on: Mar 15, 2019
 *      Author: Richard
 */

#include <stdbool.h>
#include "state.h"
#include "hardwaredefs.h"
#include "print.h"
#include "pwm.h"
#include "servo.h"

volatile float u_x;
volatile float u_y;

void control_algorithm(){
    /* Servo angle: 100 = middle
     *  446 wide =
     *  339 tall =
     *  15 px per cm, 1500 px per m
     */

    if(!context.joystick_mode){
        // Convert to m
          context.pos_x_curr = context.pos_x_curr / 750;
          context.pos_y_curr = context.pos_y_curr / 750;

          // Determine velocities
          float xdot = (context.pos_x_curr - context.pos_x_prev) / 0.03030;
          float ydot = (context.pos_y_curr - context.pos_y_prev) / 0.03030;

          // U = kx matrix multiplication
          /* Seems to slowly centre it with some SSE */
      //     u_x = -41*context.pos_x_curr - 0.5*xdot - 4*context.pos_y_curr - 9*ydot;
      //     u_y = -0.5*context.pos_x_curr - 11.5*xdot - 11.2*context.pos_y_curr - 0.14*ydot;

           u_x = context.gain[0]*context.pos_x_curr + context.gain[1]*xdot + context.gain[2]*context.pos_y_curr + context.gain[3]*ydot;
           u_y = context.gain[4]*context.pos_x_curr + context.gain[5]*xdot + context.gain[6]*context.pos_y_curr + context.gain[7]*ydot;

           //        float u_x = -21*context.pos_x_curr - 0.5*xdot - 1*context.pos_y_curr - 9*ydot;
      //        float u_y = -0.5*context.pos_x_curr - 1.5*xdot - 1.2*context.pos_y_curr - 0.14*ydot;



          /* Apr 2, 7:10 PM works ok ish with 120, 90 midpoint on x,y */
      //    u_x = -3685*context.pos_x_curr - 190*xdot - 0*context.pos_y_curr - 0*ydot;
      //    u_y = 0*context.pos_x_curr - 0*xdot + -3685*context.pos_y_curr -190*ydot;

      //     u_x = (u_x > 30) ? 30 : u_x;
      //     u_x = (u_x < -30) ? -30 : u_x;
      //     u_y = (u_y > 30) ? 30 : u_y;
      //     u_y = (u_y < -30) ? -30 : u_y;

          // angle to servo value
          context.angle_x_curr = 90 - u_x;
          context.angle_y_curr = 90 - u_y;


           //Set the angles
          /* TODO: if context.button flag is pressed, update based on the joystick values instead of angle_x_curr */
          set_angle(SERVO_X_PORT, context.angle_x_curr);
          set_angle(SERVO_Y_PORT, context.angle_y_curr);

      //    set_angle(SERVO_X_PORT, context.pos_x_curr);
      //    set_angle(SERVO_Y_PORT, context.pos_y_curr);

          // Update the positions
          context.pos_x_prev = context.pos_x_curr;
          context.pos_y_prev = context.pos_y_curr;
    }
}
