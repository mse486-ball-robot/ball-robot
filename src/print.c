/*
 * print.c
 *
 *  Created on: Feb 5, 2019
 *      Author: Richard
 */
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "hw_memmap.h"
#include "uart.h"

/* Print line
 *  - simple line printer
 */
void println(char * buf){
    uint32_t i;
    for(i = 0; i < strlen(buf); i++){
        UARTCharPut(UART0_BASE, buf[i]);
    }
    UARTCharPut(UART0_BASE, '\r');
    UARTCharPut(UART0_BASE, '\n');
}
