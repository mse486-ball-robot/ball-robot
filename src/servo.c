/*
 * servo.c
 *
 *  Created on: Mar 15, 2019
 *      Author: Richard
 */

#include <stdint.h>
#include <stdbool.h>
#include "hw_memmap.h"
#include "hw_types.h"
#include "sysctl.h"
#include "gpio.h"
#include "pin_map.h"
#include "pwm.h"
#include "hw_gpio.h"
#include "servo.h"
#include "hardwaredefs.h"

/* Angle range: 0 - 90
 * PWM clk: 1 250 000 Hz
 * PWM frequency: 50 Hz
 * PWM count min: pwm_preload * 0.05
 * PWM count max: pwm_preload * 0.1
 */

#define PWM_FREQUENCY 50


volatile uint32_t pwm_preload;
volatile uint32_t pwm_clk;

#define MAX (pwm_preload * 0.1)
#define MID (pwm_preload * 0.075)
#define MIN (pwm_preload * 0.05)
#define RANGE (MAX - MIN)

void setup_pwm(){
    // Servo PWM Setup (PD0)
    SysCtlPWMClockSet(SYSCTL_PWMDIV_8);            // PWM Clk = 80 MHz / 64 = 1.25 MHz
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);     // Servos
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);    // PWM Pin co-location
    GPIOPinTypePWM(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1);    // D0 = PWM, D1 = PWM
    GPIOPinConfigure(GPIO_PD0_M1PWM0);  // D0
    GPIOPinConfigure(GPIO_PD1_M1PWM1);  // D1

    /* 781 = 64
     * 1562 = 32
     * 3124 = 16
     * 6248 = 8
     */

    // PWM Preload
    pwm_clk = SysCtlClockGet() / 8;    // 781250 Hz
    pwm_preload = pwm_clk/PWM_FREQUENCY - 1;    // 15624
    PWMGenConfigure(SERVO_PWM_MODULE, PWM_GEN_0, PWM_GEN_MODE_DOWN);
    PWMGenPeriodSet(SERVO_PWM_MODULE, PWM_GEN_0, pwm_preload);

    // Setup X axis
    PWMPulseWidthSet(SERVO_PWM_MODULE, SERVO_X_PORT, MID);
    PWMOutputState(SERVO_PWM_MODULE, PWM_OUT_0_BIT, true);

    // Setup Y axis
    PWMPulseWidthSet(SERVO_PWM_MODULE, SERVO_Y_PORT, MID);
    PWMOutputState(SERVO_PWM_MODULE, PWM_OUT_1_BIT, true);

    PWMGenEnable(SERVO_PWM_MODULE, PWM_GEN_0);
}

/* Set angle
 *  - set the angle between 0 and 90 degrees
 */
void set_angle(uint32_t ui32PWMOut, uint32_t adjust){
    adjust = (adjust > 270) ? 270 : adjust;   // clip any angles that are too large
    volatile uint32_t minimum = pwm_clk;
    volatile uint32_t range = 0.1 * pwm_clk - minimum;

    /* 781 = one extreme, 2*781 = another extreme for 64 divide*/

//    PWMPulseWidthSet(SERVO_PWM_MODULE, ui32PWMOut, minimum + adjust * (range/180));
    PWMPulseWidthSet(SERVO_PWM_MODULE, ui32PWMOut, 6250 + (6250/270)*adjust);
//    PWMPulseWidthSet(SERVO_PWM_MODULE, ui32PWMOut, 26000);

}
