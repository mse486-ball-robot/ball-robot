/*
 * state.c
 *
 *  Created on: Mar 15, 2019
 *      Author: Richard
 */
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include "state.h"
#include "control.h"
#include "hw_memmap.h"
#include "hw_types.h"
#include "gpio.h"
#include "pin_map.h"
#include "uart.h"
#include "print.h"
#include "hw_gpio.h"
#include "sysctl.h"
#include "adc.h"
#include "printf.h"
#include "servo.h"
#include "hardwaredefs.h"
#include "pwm.h"
#include "adc_ballplate.h"

#define CAM_TICK_THRESH 3000
#define JOY_TICK_THRESH 30

/* Globals */
volatile uint8_t state = IDLE_STATE;
volatile uint8_t next_state = NONE;
context_t context;
volatile uint32_t state_ticks;
volatile bool button_locked;

/* Context initializer */
void init_context(){
    state_ticks = 0;
    button_locked = 0;
    context.angle_x_curr = 90;
    context.angle_y_curr = 90;
    context.pos_x_prev = 0;
    context.pos_y_prev = 0;
    context.pos_x_curr = 0;
    context.pos_y_curr = 0;
    context.buf_idx = 0;

        context.gain[0] = -35;
         context.gain[1] = 39.5;
         context.gain[2] = -19.0;
         context.gain[3] = -9;
         context.gain[4] = 27.5;
         context.gain[5] = -11.5;
         context.gain[6] = -35.2;
         context.gain[7] = 19.86;
    context.idx = 0;
    context.x_joystick = 0;
    context.y_joystick = 0;
    context.joystick_mode = 0;
    context.x_offset = 90;
    context.y_offset = 90;
}

/* Context buffer appender */
void context_buf_append(uint8_t byte){
    context.buf[context.buf_idx] = byte;
    context.buf_idx++;
}

/* State Functions and other Declarations */
void idle_state();
void parse_state();
void update_state();
void check_next_state();
void button_state();

/* Main state loop */
void state_loop(){
    switch(state){
        case IDLE_STATE:
            idle_state();
            break;
        case PARSE_STATE:
            parse_state();
            break;
        case UPDATE_STATE:
            update_state();
            break;
        case JOYSTICK_STATE:
            button_state();
            break;
        default: idle_state();
    }
    check_next_state();
}

/* Change State
 *      - sets the next state flag so that it can be switched into upon the next run of the state loop
 */
void change_state(uint8_t state_to_enter){
    next_state = state_to_enter;
}

/* check_next_state
 *      - Executes a state transition, if required
 */
void check_next_state(){
    if(next_state != NONE){
        // State change is requested
        state = next_state;
        next_state = NONE;
    }
}


/* State Functions */
void idle_state(){
    read_adc();

    if (GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_4) == 0 && state_ticks == 0){      /* SW1 */
        button_locked = 1;
        context.joystick_mode = (context.joystick_mode == 1) ? 0 : 1;   // Toggle joystick mode
    }

    // Switch to joystick mode servo update if we are in joystick mode
    if(context.joystick_mode == 1){
        change_state(JOYSTICK_STATE);
    }

    // Debounce the button using a simple lock
    if(button_locked) state_ticks++;

    // Different tick thresholds depending on the state path
    if((state_ticks > JOY_TICK_THRESH && context.joystick_mode == 1) ||  (state_ticks > CAM_TICK_THRESH && context.joystick_mode == 0)){
        button_locked = 0;
        state_ticks = 0;
    }
}

/* Parser State
 *  - parses the vision system UART RX buffer to pull out the coordinates
 *  - skips updating the context if there's a problem with the parsing
 */
void parse_state(){
    char xraw[5] = {0};
    char yraw[5] = {0};
    const char toks[2] = {','};
    char *token;
    bool parsing_ok = true;                                     // Flips to false if there's a parsing error

    if(context.buf[0] == '('){                                  // first element is valid
        token = strtok((char *)&context.buf[1], toks);

        // First element - x position
        if(token != NULL) strcpy(xraw, token);
        else parsing_ok = false;

        // Second element - y position
        token = strtok(NULL, toks);
        if(token != NULL) strcpy(yraw, token);
        else parsing_ok = false;

        // TODO: more elements

        // Do the integer conversion if everything is ok
        if(parsing_ok == true){
            context.pos_x_curr = strtof((const char *)xraw, NULL);
            context.pos_y_curr = strtof((const char *)yraw, NULL);
        }
    }
    else{                                                   // First element is not valid :(
        parsing_ok = false;
    }

    /* Update state based on how things went */
    if(parsing_ok == true) change_state(UPDATE_STATE);       // everything's good
    else change_state(IDLE_STATE);                           // there was a problem, just ignore it

    /* Update context flags */
    context.buf_idx = 0;
}

void button_state(){
    if (context.joystick_mode == 1){
         set_angle(SERVO_X_PORT, 90 - ((context.x_joystick -2100)/40));
         set_angle(SERVO_Y_PORT, 90 - ((context.y_joystick -2100)/40));
     }
}

void update_state(){
    control_algorithm();     // update the control algorithm
    change_state(IDLE_STATE);
}
