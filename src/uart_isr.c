/*
 * uart_isr.c
 *
 *  Created on: Feb 5, 2019
 *      Author: Richard
 */
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "hw_memmap.h"
#include "hw_types.h"
#include "hw_ints.h"
#include "sysctl.h"
#include "interrupt.h"
#include "gpio.h"
#include "pin_map.h"
#include "uart.h"
#include "isr.h"
#include "state.h"

// Position variables for the ball
volatile uint32_t x;
volatile uint32_t y;

/* Preconditions: UART is set up
 * Example: http://software-dl.ti.com/trainingTTO/trainingTTO_public_sw/GSW-TM4C123G-LaunchPad/TM4C123G_LaunchPad_Workshop_Workbook.pdf (section 12-13)
 */
void uart0_isr(void){
    uint32_t status;

    status = UARTIntStatus(UART0_BASE, true);   //get interrupt status
    UARTIntClear(UART0_BASE, status);           //clear the asserted interrupts
    while(UARTCharsAvail(UART0_BASE)){
        char val = UARTCharGetNonBlocking(UART0_BASE);
        if(val == 'q'){
            context.idx = 0;
        }
        if(val == 'w'){
            context.idx = 1;
        }
        if(val == 'e'){
              context.idx = 2;
        }
        if(val == 'r'){
              context.idx = 3;
        }
        if(val == 'z'){
            context.idx = 4;
        }
        if(val == 'x'){
            context.idx = 5;
        }
        if(val == 'c'){
              context.idx = 6;
        }
        if(val == 'v'){
              context.idx = 7;
        }

        if( val == 'u'){
            context.gain[context.idx] += 1;
        }
        else if(val == 'd'){
            context.gain[context.idx] -= 1;
        }
        UARTCharPutNonBlocking(UART0_BASE, val); // send it back out
    }
}


void uart1_isr_state(void){
    static volatile bool triggered = false;
    uint8_t byte;

    uint32_t status = UARTIntStatus(UART1_BASE, true);   // get interrupt status
       UARTIntClear(UART1_BASE, status);                 // clear the asserted interrupts

       /* Wait until start of a data set comes in "("
        *  - triggered and x will be processed
        */
       while(UARTCharsAvail(UART1_BASE)){
           byte = UARTCharGetNonBlocking(UART1_BASE);

//           UARTCharPutNonBlocking(UART0_BASE, byte); // send it back out

           // Start of packet, initialize
           if(byte == '('){
               triggered = true;
           }
           // end of packet
           else if(byte == ')'){
               triggered = false;
//               context_buf_append(byte);  // add the last byte manually
               change_state(PARSE_STATE);
               return;
           }

           if(triggered){
               context_buf_append(byte);  // slam bytes into the buffer
           }
       }
}

/* UART1
 *  - used to receive from the pi
 */
void uart1_isr(void){
    volatile uint32_t status;
    volatile char byte;

    static volatile bool triggered;
    static volatile bool x_flag;
    static volatile uint8_t xpos;
    static volatile uint8_t ypos;
    static volatile char xraw[5];
    static volatile char yraw[5];

    status = UARTIntStatus(UART1_BASE, true);   //get interrupt status
    UARTIntClear(UART1_BASE, status);           //clear the asserted interrupts

    /* Wait until start of a data set comes in "("
     *  - triggered and x will be processed
     *  - wait until "," indicating y is next
     *  - wait until ")" indicating end of packet
     *
     *  SUPPORTS MAX 4 DIGITS
     */
    while(UARTCharsAvail(UART1_BASE)){
        byte = UARTCharGetNonBlocking(UART1_BASE);

        UARTCharPutNonBlocking(UART0_BASE, byte); // send it back out

        // Start of packet, initialize
        if(byte == '('){
            triggered = true;
            x_flag = true;
            xpos = 0;
            ypos = 0;
        }

        // Separator between x and y
        else if(byte == ','){
            x_flag = false;
        }

        // End of packet reset states and convert to integer
        else if(byte == ')'){
            triggered = false;
            x_flag = false;

            // Add null to end of valid data so strtoi is happy (positions are always incremented on valid data)
            xraw[xpos] = '\0';
            yraw[ypos] = '\0';
            x = strtol((const char *)xraw, NULL, 10);
            y = strtol((const char *)yraw, NULL, 10);
        }

        // Handle the x coordinate
        else if(triggered && x_flag && (byte >= '0' || byte <= '9')){
            xraw[xpos] = byte;
            xpos++;
        }

        // Handle the y coordinate
        else if(triggered && ~x_flag && (byte >= '0' || byte <= '9')){
            yraw[ypos] = byte;
            ypos++;
        }
    }
}
